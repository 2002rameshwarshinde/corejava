
public class BinomialExpansion {
	public static void main(String[] args) {
		
		BinomialExpansion.equation2(10,20);
		BinomialExpansion.equation3(10,10);//parametrised function//function call
		BinomialExpansion.equation4(20,30);
		BinomialExpansion.equation5(10,10);

	}

	public static void equation2(int a,int b) {//user defined function
		
		int c = a * a + 2 * a * b + b * b;
		System.out.println(c);
	}

	public static void equation3(int a,int b) {
		
		int c = a * a * a + 3 * a * a * b + 3 * a * b * b + b * b * b;
		System.out.println(c);

	}

	public static void equation4(int a,int b) {
				int c = a * a * a * a + 4 * a * a * a * b + 6 * a * a * b * b + 4 * a * b * b * b + b * b * b * b;
		System.out.println(c);
	}

	public static void equation5(int a,int b) {
				int c = a * a * a * a * a + 5 * a * a * a * a * b + 10 * a * a * a * b * b + 10 * a * a * b * b * b
				+ 5 * a * b * b * b * b + b * b * b * b * b;
		System.out.println(c);
	}

}