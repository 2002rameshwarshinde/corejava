package Fileoprations;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class CopyFile {
	public static void main(String args[]) throws IOException {
		Scanner sc=new Scanner(System.in);
		System.out.print("provide Filepath: ");
		String Filepath=sc.nextLine();
		File file=new File(Filepath);
		FileReader fr=new FileReader(file);
		BufferedReader br=new BufferedReader(fr);
		String newfilepath="C:\\Users\\HP\\eclipse-workspace\\CoreJava\\src\\Fileoprations\\Writing";
		File newfile=new File(newfilepath);
		FileWriter fw=new FileWriter(newfile);
		BufferedWriter bw=new BufferedWriter(fw);
		String line;
		while((line=br.readLine())!=null) {
			
			bw.write(line);
			bw.write("\n");
		}
		bw.close();
		fw.close();
		fr.close();
		br.close(); 
	}

}
